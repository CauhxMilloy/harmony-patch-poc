# Harmony Patch PoC

Proof of concept using Harmony (https://github.com/pardeike/Harmony).

Runs perfectly on Windows, does not patch on linux. **SEE [BELOW](#solution) FOR SOLUTION TO PROBLEM**.

Run nuget restore (to get Harmony dll). Build solution.
TBH, the project/solution is just a basic template one from Visual Studio.
There is nothing special about it.
Feel free to just copy the Program.cs part.

With the .exe built. Running it on windows runs fine (with Harmony patching).
However, running it (the same exact .exe file) on linux does not have patching do anything.

Sample output:

Windows (7):
```> ConsoleApp7.exe
Getter val before patch: 0
raw val: 0
patching getter...
~~~~In Get patch
~~~~Patch for: GetX
Getter val after patch: 42
raw val: 0

Calling Setter (777)
raw val: 777
patching setter...
Calling Setter (-444)
~~~~In Set patch
~~~~Patch for: SetX
raw val: 777
```

Linux (Ubuntu 16.04):
```Getter val before patch: 0
raw val: 0
patching getter...
Getter val after patch: 0
raw val: 0

Calling Setter (777)
raw val: 777
patching setter...
Calling Setter (-444)
raw val: -444
```

### Solution

It seems that linux's mono runtime is much more aggressive about inlining than the Window .net runtime.
If the targetted method is inlined, it does not allow Harmony's IL patching to function -- as the method essentially does not exist any longer.
There are two potential fixes for this, which will get the patched prefix methods working on linux.

#### Use Attributes

Adding `[MethodImpl(MethodImplOptions.NoInlining)]` to the patched method will prevent it from being inlined.
This will, not surprisingly, prevent inlining on the given function.
Harmony will patch the function, and .net will call into the patched IL as expected.
The downside of this is the requirement that you can add the attribute to the source code.

#### Use Command-line Args

Passing `--optimize=-inline` as a command-line argument to mono will disable method inlining.
This is a broad stroke that might have other side-effects on your application.
However, it does get around the issue of not being able to add the `MethodImpl` attribute to the source code.

