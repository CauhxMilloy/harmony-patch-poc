﻿
using System;
using System.Reflection;
using Harmony;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            HarmonyInstance h = HarmonyInstance.Create("com.example.thing");

            CClassToPatch c = new CClassToPatch();

            Console.WriteLine(String.Format("Getter val before patch: {0}", c.GetX()));
            Console.WriteLine(String.Format("raw val: {0}", c.x));

            Console.WriteLine("patching getter...");
            MethodInfo mg = typeof(CClassToPatch).GetMethod("GetX");
            MethodInfo pg = typeof(Program).GetMethod("PrefixGet");
            h.Patch(mg, new HarmonyMethod(pg));

            Console.WriteLine(String.Format("Getter val after patch: {0}", c.GetX()));
            Console.WriteLine(String.Format("raw val: {0}", c.x));

            Console.WriteLine();

            Console.WriteLine("Calling Setter (777)");
            c.SetX(777);
            Console.WriteLine(String.Format("raw val: {0}", c.x));

            Console.WriteLine("patching setter...");
            MethodInfo ms = typeof(CClassToPatch).GetMethod("SetX");
            MethodInfo ps = typeof(Program).GetMethod("PrefixSet");
            h.Patch(ms, new HarmonyMethod(ps));

            Console.WriteLine("Calling Setter (-444)");
            c.SetX(-444);
            Console.WriteLine(String.Format("raw val: {0}", c.x));
        }

        public class CClassToPatch
        {
            public int GetX()
            {
                return x;
            }
            public void SetX(int val)
            {
                x = val;
            }

            public int x;
        }

        public static bool PrefixGet(
            MethodInfo __originalMethod,
            CClassToPatch __instance,
            ref int __result)
        {
            Console.WriteLine("~~~~In Get patch");
            Console.WriteLine(String.Format("~~~~Patch for: {0}", __originalMethod.Name));
            __result = 42;

            return false;
        }
        public static bool PrefixSet(
            MethodInfo __originalMethod,
            CClassToPatch __instance,
            int __0)
        {
            Console.WriteLine("~~~~In Set patch");
            Console.WriteLine(String.Format("~~~~Patch for: {0}", __originalMethod.Name));

            return false;
        }
    }
}
